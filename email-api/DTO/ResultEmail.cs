﻿namespace email_api.DTO
{
    public class ResultEmail
    {
        public string nome { get; set; }
        public string email { get; set; }
        public string status { get; set; }
    }
}
