﻿using email_api.DTO;
using MailKit.Net.Smtp;
using MimeKit;

namespace email_api.Servicos
{
    public interface IServEnviaEmail
    {
        bool EnviaEmail(MailData mailData);
    }
    public class ServEnviaEmail: IServEnviaEmail
    {
        public bool EnviaEmail(MailData mailData)
        {
            try
            {
                using (MimeMessage emailMessage = new MimeMessage())
                {
                    MailboxAddress emailFrom = new MailboxAddress("BOLETO", "BOLETO@BOLETO.COM");
                    emailMessage.From.Add(emailFrom);

                    MailboxAddress emailTo = new MailboxAddress(mailData.EmailToName, mailData.EmailToId);
                    emailMessage.To.Add(emailTo);

                    emailMessage.Subject = mailData.EmailSubject;

                    BodyBuilder emailBodyBuilder = new BodyBuilder();
                    emailBodyBuilder.TextBody = mailData.EmailBody;

                    emailMessage.Body = emailBodyBuilder.ToMessageBody();

                    using (SmtpClient mailClient = new SmtpClient())
                    {
                        mailClient.Connect("sandbox.smtp.mailtrap.io", 587, MailKit.Security.SecureSocketOptions.StartTls);
                        mailClient.Authenticate("2f385019caa0c6", "fe75d276a42d4f");
                        mailClient.Send(emailMessage);
                        mailClient.Disconnect(true);
                    }
                }

                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
    }
}
