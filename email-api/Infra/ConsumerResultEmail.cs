﻿using email_api.DTO;
using email_api.Servicos;
using Newtonsoft.Json;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using System.Text;

namespace email_api.Infra
{
    public class ConsumerResultEmail
    {
        public static void ResultEmail(IModel channel) 
        {
            var nomeFila = "email";

            channel.QueueDeclare(queue: nomeFila, durable: true, exclusive: false, autoDelete: false);

            var consumer = new EventingBasicConsumer(channel);

            consumer.Received += (model, ea) =>
            {
                var body = ea.Body.ToArray();
                var message = Encoding.UTF8.GetString(body);

                var resultInformarPromocaoDto = JsonConvert.DeserializeObject<ResultEmail>(message);

                var servEnviaEmail = GeradorDeServicos.ServiceProvider.GetService<IServEnviaEmail>();

                var data = new MailData { 
                    EmailBody="Seu boleto foi compensado com sucesso!", 
                    EmailSubject="Boleto compensado", 
                    EmailToId=resultInformarPromocaoDto.email, 
                    EmailToName=resultInformarPromocaoDto.nome 
                };

                servEnviaEmail.EnviaEmail(data);
            };

            channel.BasicConsume(queue: nomeFila,
                     autoAck: true,
                     consumer: consumer);
        }
    }
}
